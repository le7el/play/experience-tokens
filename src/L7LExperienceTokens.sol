// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import {ERC1155} from "solmate/tokens/ERC1155.sol";
import "./interface/IERC1155Mintable.sol";
import "./interface/IERC1155Burnable.sol";
import "./interface/IERC1404Controller.sol";
import "./ERC1404Controller.sol";

/**
 * @dev Experience points are ERC1155 fungible tokens, which also comply a slightly modified ERC1404
 *      transfer restriction standard. It has a huge gas overhead for transfers, but it shouldn't be an issue
 *      because the majority of users will only mint and burn these tokens.
 */
contract L7LExperienceTokens is ERC1155, IERC1155Mintable, IERC1155Burnable {
    uint8 internal constant SUCCESS_CODE = 1;

    mapping(uint256 => uint256) public totalSupply;

    address public trustedMinter;
    address public trustedBurner;
    IERC1404Controller public trustedController; 

    /**
     * @dev Can only be a minter.
     */
    modifier onlyMinter() {
        require(msg.sender == trustedMinter, "Not a minter");
        _;
    }

    /**
     * @dev Can only be a owner of tokens, burner or an approved address.
     * @param _account Owner of tokens.
     */
    modifier onlyOwnerOrBurnerOrApproved(address _account) {
        require(
            _account == msg.sender || isApprovedForAll[_account][msg.sender] || msg.sender == trustedBurner,
            "ERC1155: caller is not token owner, approved contract or burner"
        );
        _;
    }

    /**
     * @dev It's expected that the deployer wallet changes minting rights to multisig or MultiMinter contract.
     */
    constructor() {
        trustedMinter = msg.sender;
        trustedBurner = msg.sender;
        trustedController = new ERC1404Controller(msg.sender);
    }

    /**
     * @dev Metadata for tokens, based on it's id.
     */
    function uri(uint256) public pure override returns (string memory) {
        return "https://le7el.com/v1/exptokens/{id}.json";
    }

    /**
     * @dev Change address of minter.
     * @param _newMinter Address of a new minter contract or wallet.
     */
    function changeMinter(address _newMinter) external override onlyMinter {
        trustedMinter = _newMinter;
    }

    /**
     * @dev Change address of burner.
     * @param _newBurner Address of a new burner contract or wallet.
     */
    function changeBurner(address _newBurner) external override {
        require(msg.sender == trustedBurner, "Not a burner");
        trustedBurner = _newBurner;
    }

    /**
     * @dev Change address of ERC1404 transfer controller.
     * @param _newController Address of a new controller contract.
     */
    function changeController(address _newController) external override {
        require(msg.sender == address(trustedController), "Not a controller");
        trustedController = IERC1404Controller(_newController);
    }

    /**
     * @dev Makes default tranfer implementation pausible.
     */
    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _id,
        uint256 _amount,
        bytes calldata _data
    ) public override {
        require(trustedController.detectTransferRestriction(_from, _to, _id) == SUCCESS_CODE, "Not allowed");
        super.safeTransferFrom(_from, _to, _id, _amount, _data);
        if (_to == address(0)) totalSupply[_id] -= _amount;
    }

    /**
     * @dev Makes default tranfer implementation pausible.
     */
    function safeBatchTransferFrom(
        address _from,
        address _to,
        uint256[] calldata _ids,
        uint256[] calldata _amounts,
        bytes calldata _data
    ) public override {
        require(trustedController.detectTransferRestriction(_from, _to, _ids) == SUCCESS_CODE, "Not allowed");
        super.safeBatchTransferFrom(_from, _to, _ids, _amounts, _data);
        if (_to == address(0)) _registerBatchBurn(_ids, _amounts);
    }

    /**
     * @dev Mint amount of id tokens to address.
     * @param _to Address where the newly minted tokens will be allocated.
     * @param _id Id of token to be minted.
     * @param _amount Amount of tokens to be minted.
     * @param _data Metadata.
     */
    function mint(address _to, uint256 _id, uint256 _amount, bytes memory _data) external override onlyMinter {
        require(_id < 256, "id overflow");
        totalSupply[_id] += _amount;
        _mint(_to, _id, _amount, _data);
    }

    /**
     * @dev Mint amount of id tokens to address in a batch.
     * @param _to Address where the newly minted tokens will be allocated.
     * @param _ids Ids of tokens to be minted.
     * @param _amounts Amounts of tokens to be minted.
     * @param _data Metadata.
     */
    function mintBatch(
        address _to,
        uint256[] memory _ids,
        uint256[] memory _amounts,
        bytes memory _data
    ) external override onlyMinter {
        for (uint256 _i = 0; _i < _ids.length;) {
            uint256 _id = _ids[_i];
            require(_id < 256, "id overflow");
            totalSupply[_id] += _amounts[_i];

            // An array can't have a total length
            // larger than the max uint256 value.
            unchecked {
                ++_i;
            }
        }
        _batchMint(_to, _ids, _amounts, _data);
    }

    /**
     * @dev Burn token from account. Owner of tokens or approved contract can do the burn.
     * @param _account Address which will get it's tokens burn.
     * @param _id Id of token to be burned.
     * @param _amount Amount of tokens to be burned.
     */
    function burn(
        address _account,
        uint256 _id,
        uint256 _amount
    ) external override onlyOwnerOrBurnerOrApproved(_account) {
        _burn(_account, _id, _amount);
        totalSupply[_id] -= _amount;
    }

    /**
     * @dev Burn tokens from account in a batch. Owner of tokens or approved contract can do the burn.
     * @param _account Address which will get it's tokens burn.
     * @param _ids Ids of tokens to be burned.
     * @param _amounts Amounts of tokens to be burned.
     */
    function burnBatch(
        address _account,
        uint256[] memory _ids,
        uint256[] memory _amounts
    ) external override onlyOwnerOrBurnerOrApproved(_account) {
        _batchBurn(_account, _ids, _amounts);
        _registerBatchBurn(_ids, _amounts);
    }

    /**
     * @dev Reduce total supply based on the burn params.
     * @param _ids Ids of tokens to be burned.
     * @param _amounts Amounts of tokens to be burned.
     */
    function _registerBatchBurn(uint256[] memory _ids, uint256[] memory _amounts) internal {
        for (uint256 _i = 0; _i < _ids.length;) {
            totalSupply[_ids[_i]] -= _amounts[_i];

            // An array can't have a total length
            // larger than the max uint256 value.
            unchecked {
                ++_i;
            }
        }
    }
}
