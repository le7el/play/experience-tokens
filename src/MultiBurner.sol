// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "@openzeppelin/security/Pausable.sol";
import './interface/IERC1155Burnable.sol';

/**
 * @dev Adapted version of Aragon's multi-minter smart contract
 *      ref. https://github.com/aragon/aragon-network-token/blob/master/packages/v2/contracts/ANTv2MultiMinter.sol
 *      The added feature is that burning can be paused and permission for burning specific ERC1155 id can be
 *      granted separately.
 */
contract MultiBurner is Pausable {
    string private constant ERROR_NOT_OWNER = "Not owner";
    string private constant ERROR_NOT_BURNER = "Not burner";

    address public owner;
    IERC1155Burnable public burnable;

    mapping (address => mapping (uint256 => bool)) public canBurn;

    event AddedBurner(address indexed burner, uint256 indexed id);
    event RemovedBurner(address indexed burner, uint256 indexed id);
    event ChangedOwner(address indexed newOwner);

    modifier onlyOwner {
        require(msg.sender == owner, ERROR_NOT_OWNER);
        _;
    }

    modifier onlyBurner(uint256 _id) {
        require(canBurn[msg.sender][_id] || msg.sender == owner, ERROR_NOT_BURNER);
        _;
    }

    constructor(address _owner, IERC1155Burnable _burnable) {
        owner = _owner;
        burnable = _burnable;
    }

    /**
     * @dev Burn amount of tokens from the address for ERC1155 multi-token standard.
     * @param _to Address of token owner.
     * @param _id Token id which is burned.
     * @param _amount Amount of burned tokens.
     */
    function burn(address _to, uint256 _id, uint256 _amount) external whenNotPaused onlyBurner(_id) {
        burnable.burn(_to, _id, _amount);
    }

    /**
     * @dev Mint amount of tokens to the address for ERC1155 multi-token standard.
     * @param _to Address of token owner.
     * @param _ids Token id which is burned.
     * @param _amounts Amount of burned tokens.
     */
    function burnBatch(address _to, uint256[] memory _ids, uint256[] memory _amounts) external whenNotPaused {
        if (msg.sender != owner) {
            for (uint256 _i = 0; _i < _ids.length;) {
                require(canBurn[msg.sender][_ids[_i]], ERROR_NOT_BURNER);

                // An array can't have a total length
                // larger than the max uint256 value.
                unchecked {
                    ++_i;
                }
            }
        }
        burnable.burnBatch(_to, _ids, _amounts);
    }

    /**
     * @dev Enable burning rights for some address.
     * @param _burner Address of a new burner.
     * @param _id Token id which is allowed to burn.
     */
    function addBurner(address _burner, uint256 _id) external onlyOwner {
        canBurn[_burner][_id] = true;
        emit AddedBurner(_burner, _id);
    }

    /**
     * @dev Disable burning rights for some address.
     * @param _burner Address of a current burner.
     */
    function removeBurner(address _burner, uint256 _id) external onlyOwner {
        canBurn[_burner][_id] = false;
        emit RemovedBurner(_burner, _id);
    }

    /**
     * @dev Change address of burner on the token contract.
     * @param _newBurner Address of a new burner contract or wallet.
     */
    function changeBurnableBurner(address _newBurner) onlyOwner external {
        burnable.changeBurner(_newBurner);
    }

    /**
     * @dev Change owner address which can assign the new burners.
     * @param _newOwner Address of a new owner.
     */
    function changeOwner(address _newOwner) onlyOwner external {
        owner = _newOwner;
        emit ChangedOwner(_newOwner);
    }

    /**
     * @dev Pauses all burns.
     */
    function pause() public onlyOwner {
        _pause();
    }

    /**
     * @dev Unpauses all burns.
     */
    function unpause() public onlyOwner {
        _unpause();
    }
}