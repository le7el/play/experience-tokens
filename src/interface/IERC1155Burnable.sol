// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

/**
 * @dev ERC1155 token proxy with burnable API.
 */
abstract contract IERC1155Burnable {
    /**
     * @dev ERC1155 which supports burning from multiple addresses.
     * @param _to Address where the newly burned tokens will be allocated.
     * @param _id Id of token to be burned.
     * @param _amount Amount of tokens to be burned.
     */
    function burn(address _to, uint256 _id, uint256 _amount) virtual external;

    /**
     * @dev ERC1155 which supports burning from multiple addresses in a batch.
     * @param _to Address where the newly burned tokens will be allocated.
     * @param _ids Ids of tokens to be burned.
     * @param _amounts Amount of tokens to be burned.
     */
    function burnBatch(address _to, uint256[] memory _ids, uint256[] memory _amounts) virtual external;

    /**
     * @dev Change address of burner on the token contract.
     * @param _newBurner Address of a new burner contract or wallet.
     */
    function changeBurner(address _newBurner) virtual external;
}