// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

/**
 * @dev ERC1155 token proxy with mintable API.
 */
abstract contract IERC1155Mintable {
    /**
     * @dev ERC1155 which supports minting from multiple addresses.
     * @param _to Address where the newly minted tokens will be allocated.
     * @param _id Id of token to be minted.
     * @param _amount Amount of tokens to be minted.
     * @param _data Metadata.
     */
    function mint(address _to, uint256 _id, uint256 _amount, bytes memory _data) virtual external;

    /**
     * @dev ERC1155 which supports minting from multiple addresses in a batch.
     * @param _to Address where the newly minted tokens will be allocated.
     * @param _ids Ids of tokens to be minted.
     * @param _amounts Amount of tokens to be minted.
     * @param _data Metadata.
     */
    function mintBatch(address _to, uint256[] memory _ids, uint256[] memory _amounts, bytes memory _data) virtual external;

    /**
     * @dev Change address of minter on the token contract.
     * @param _newMinter Address of a new minter contract or wallet.
     */
    function changeMinter(address _newMinter) virtual external;

    /**
     * @dev Change address of controller contract which manages whitelists.
     * @param _newController Address of a new controller contract.
     */
    function changeController(address _newController) virtual external;
}