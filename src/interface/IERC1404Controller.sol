// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

/**
 * @dev ERC1404 manager API to prevent token transfers.
 */
abstract contract IERC1404Controller {
    /**
     * @dev Experience tokens are intended to be burned for leveling, transfers are usually restricted.
     * @param _from Address which tries to transfer.
     * @param _to Reciepient address.
     * @param _id ERC1155 token id.
     * @return Code by which to reference message for rejection reasoning.
     */
    function detectTransferRestriction(address _from, address _to, uint256 _id) public view virtual returns (uint8);

    /**
     * @dev Experience tokens are intended to be burned for leveling, transfers are usually restricted.
     * @param _from Address which tries to transfer.
     * @param _to Reciepient address.
     * @param _ids ERC1155 token ids.
     * @return Code by which to reference message for rejection reasoning.
     */
    function detectTransferRestriction(address _from, address _to, uint256[] calldata _ids) public view virtual returns (uint8);

    /**
     * @dev Used to upgrade this controller.
     * @param _newController Address of a new controller contract.
     */
    function adminChangeController(address _newController) external virtual;
}