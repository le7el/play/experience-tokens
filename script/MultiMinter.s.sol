// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import {MultiMinter} from "src/MultiMinter.sol";
import {IERC1155Mintable} from 'src/interface/IERC1155Mintable.sol';

contract MultiMinterScript is Script {
    function setUp() public {}

    function run() public {
        vm.broadcast();
        address token_address = vm.envAddress("TOKEN");
        MultiMinter minter = new MultiMinter(msg.sender, IERC1155Mintable(token_address));
        console.log("MultiMinter: ");
        console.log(address(minter));
    }
}
