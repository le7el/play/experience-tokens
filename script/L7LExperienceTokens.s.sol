// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import {L7LExperienceTokens} from "src/L7LExperienceTokens.sol";

contract L7LExperienceTokensScript is Script {
    function setUp() public {}

    function run() public {
        vm.broadcast();
        L7LExperienceTokens tokens = new L7LExperienceTokens();
        console.log("L7LExperienceTokens: ");
        console.log(address(tokens));
    }
}
