// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.10;

import "forge-std/Script.sol";

import {MultiBurner} from "src/MultiBurner.sol";
import {IERC1155Burnable} from 'src/interface/IERC1155Burnable.sol';

contract MultiBurnerScript is Script {
    function setUp() public {}

    function run() public {
        vm.broadcast();
        address token_address = vm.envAddress("TOKEN");
        MultiBurner burner = new MultiBurner(msg.sender, IERC1155Burnable(token_address));
        console.log("MultiBurner: ");
        console.log(address(burner));
    }
}
