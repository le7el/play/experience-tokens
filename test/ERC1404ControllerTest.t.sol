// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "../src/ERC1404Controller.sol";

contract ERC1404ControllerTest is Test {
    ERC1404Controller private controller;

    address internal admin = address(1);

    function setUp() public {
        vm.label(admin, "Admin");
        controller = new ERC1404Controller(admin);
    }

    function testTransferPreventionByDefault(address _from, address _to, uint256 _id) public {
        vm.assume(_id < 256);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testBatchTransferPreventionByDefault(address _from, address _to, uint256[] calldata _ids) public {
        assertTrue(controller.detectTransferRestriction(_from, _to, _ids) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testCannotWhitelistByRandom(address _random, uint256 _id) public {
        vm.assume(_id < 256);
        vm.startPrank(_random);
        vm.expectRevert("Not a manager");
        controller.adminTokenIdWhitelisted(_id, true);
        vm.expectRevert("Not a manager");
        controller.adminTokenIdWhitelisted(_id, false);
        vm.expectRevert("Not a manager");
        controller.adminSenderTokenIdWhitelisted(_random, _id, true);
        vm.expectRevert("Not a manager");
        controller.adminSenderTokenIdWhitelisted(_random, _id, false);
        vm.expectRevert("Not a manager");
        controller.adminRecieverTokenIdWhitelisted(_random, _id, true);
        vm.expectRevert("Not a manager");
        controller.adminRecieverTokenIdWhitelisted(_random, _id, false);
        vm.stopPrank();
    }

    function testEmptyWhitelisting() public {
        vm.startPrank(admin);
        controller.adminTokenIdWhitelisted(0, true);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(0, false);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.ERROR_NOT_WHITELISTED_CODE());
        controller.adminSenderTokenIdWhitelisted(address(0), 0, true);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.SUCCESS_CODE());
        controller.adminSenderTokenIdWhitelisted(address(0), 0, false);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.ERROR_NOT_WHITELISTED_CODE());
        controller.adminRecieverTokenIdWhitelisted(address(0), 0, true);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.SUCCESS_CODE());
        controller.adminRecieverTokenIdWhitelisted(address(0), 0, false);
        assertTrue(controller.detectTransferRestriction(address(0), address(0), 0) == controller.ERROR_NOT_WHITELISTED_CODE());
        vm.stopPrank();
    }

    function testWhitelistingByFuzzId(address _from, address _to, uint256 _id) public {
        vm.assume(_id < 256);
        vm.startPrank(admin);
        controller.adminTokenIdWhitelisted(_id, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(_id, false);
        vm.stopPrank();
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testWhitelistingMultipleTokenIds(address _from, address _to) public {
        vm.startPrank(admin);
        controller.adminTokenIdWhitelisted(0, true);
        controller.adminTokenIdWhitelisted(1, true);
        controller.adminTokenIdWhitelisted(21, true);
        controller.adminTokenIdWhitelisted(33, true);
        controller.adminTokenIdWhitelisted(255, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, 0) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 1) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 21) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 33) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 255) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(21, false);
        controller.adminTokenIdWhitelisted(1, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, 0) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 1) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 21) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 33) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 255) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(0, false);
        controller.adminTokenIdWhitelisted(33, false);
        controller.adminTokenIdWhitelisted(255, false);
        vm.stopPrank();
        assertTrue(controller.detectTransferRestriction(_from, _to, 0) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 1) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 21) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 33) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_from, _to, 255) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testWhitelistingBySender(address _from, address _to, uint256 _id) public {
        vm.assume(_id < 256 && _from != _to);
        vm.startPrank(admin);
        controller.adminSenderTokenIdWhitelisted(_from, _id, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_to, _from, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        controller.adminSenderTokenIdWhitelisted(_from, _id, false);
        vm.stopPrank();
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_to, _from, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testWhitelistingByReciever(address _from, address _to, uint256 _id) public {
        vm.assume(_id < 256 && _from != _to);
        vm.startPrank(admin);
        controller.adminRecieverTokenIdWhitelisted(_to, _id, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        assertTrue(controller.detectTransferRestriction(_to, _from, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        controller.adminRecieverTokenIdWhitelisted(_to, _id, false);
        vm.stopPrank();
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        assertTrue(controller.detectTransferRestriction(_to, _from, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testWixedWhitelisting(address _from, address _to, uint256 _id) public {
        vm.assume(_id > 0 && _id < 256);
        vm.startPrank(admin);
        controller.adminSenderTokenIdWhitelisted(_from, _id, true);
        controller.adminSenderTokenIdWhitelisted(_from, _id - 1, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminRecieverTokenIdWhitelisted(_to, _id, true);
        controller.adminRecieverTokenIdWhitelisted(_to, _id - 1, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(_id, true);
        controller.adminTokenIdWhitelisted(_id - 1, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(_id, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminRecieverTokenIdWhitelisted(_to, _id, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminSenderTokenIdWhitelisted(_from, _id, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        vm.stopPrank();
    }

    function testNoChangeIfWhitelisted(address _from, address _to, uint256 _id) public {
        vm.startPrank(admin);
        controller.adminTokenIdWhitelisted(_id, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(_id, true);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.SUCCESS_CODE());
        controller.adminTokenIdWhitelisted(_id, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
        controller.adminTokenIdWhitelisted(_id, false);
        assertTrue(controller.detectTransferRestriction(_from, _to, _id) == controller.ERROR_NOT_WHITELISTED_CODE());
    }

    function testCannotChangeAdminByRandom(address _dude, address _newManager) public {
        vm.prank(_dude);
        vm.expectRevert("Not a manager");
        controller.adminChangeManager(_newManager);
    }

    function testAdminChange(address _newManager) public {
        vm.prank(admin);
        controller.adminChangeManager(_newManager);
        assertTrue(controller.trustedManager() == _newManager);
    }

    function testCannotChangeControllerByRandom(address _dude, address _newController) public {
        vm.prank(_dude);
        vm.expectRevert("Not a manager");
        controller.adminChangeController(_newController);
    }

    function testErrorCodes(uint8 _code) public {
        vm.assume(_code != 2);
        assertStringEq(controller.messageForTransferRestriction(controller.ERROR_NOT_WHITELISTED_CODE()), controller.ERROR_NOT_WHITELISTED());
        assertStringEq(controller.messageForTransferRestriction(_code), controller.ERROR_FAILURE());
    }

    function assertStringEq(string memory _str1, string memory _str2) public {
        assertTrue(keccak256(abi.encode(_str1)) == keccak256(abi.encode(_str2)));
    }
}
