// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "../src/MultiBurner.sol";
import "../src/L7LExperienceTokens.sol";

contract MultiBurnerTest is Test {
    MultiBurner private multiBurner;
    L7LExperienceTokens private token;

    address internal admin = address(1);
    address internal dude = address(2);
    address internal burner = address(3);

    uint256 internal constant MAX_INT = 115792089237316195423570985008687907853269984665640564039457584007913129639935;

    uint256[] internal ids = [0, 1, 30, 4, 255];
    uint256[] internal amounts = [0, 100, 1, MAX_INT - 1, 500];

    function setUp() public {
        vm.label(admin, "Admin");
        vm.prank(admin);
        L7LExperienceTokens _burnable = new L7LExperienceTokens();
        token = _burnable;
        vm.prank(admin);
        multiBurner = new MultiBurner(admin, _burnable);
        vm.prank(admin);
        token.changeBurner(address(multiBurner));
    }

    function testBurnWithMultiBurner(address _from, uint256 _id, uint256 _amount) public {
        // Ensure that id = 4 won't overflow as well as other values with 500 being the max increment
        vm.assume(_id < 256 && _id != 4 && _amount < MAX_INT - 500 && ERC1155CompliantReciever(_from));
        vm.prank(admin);
        token.mint(_from, _id, _amount, "");
        assertTrue(token.balanceOf(_from, _id) == _amount);
        vm.prank(admin);
        multiBurner.addBurner(burner, _id);
        vm.prank(burner);
        multiBurner.burn(_from, _id, _amount);
        assertTrue(token.balanceOf(_from, _id) == 0);
        for (uint256 _i = 0; _i < ids.length;) {
            vm.prank(admin);
            multiBurner.addBurner(burner, ids[_i]);
            _i++;
        }
        vm.prank(admin);
        token.mintBatch(_from, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            uint256 __amount = amounts[_i];
            assertTrue(token.balanceOf(_from, ids[_i]) == __amount);
            _i++;
        }
        vm.prank(burner);
        multiBurner.burnBatch(_from, ids, amounts);
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_from, ids[_i]) == 0);
            _i++;
        }
    }

    function testCannotAddAndRemovingBurnersByRandom(address _burner, uint256 _id) public {
        vm.assume(_id < 256);
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiBurner.addBurner(_burner, _id);
        assertFalse(multiBurner.canBurn(_burner, _id));
        vm.prank(admin);
        multiBurner.addBurner(_burner, _id);
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiBurner.removeBurner(_burner, _id);
        assertTrue(multiBurner.canBurn(_burner, _id));
    }

    function testAddingAndRemovingNewBurners(address _burner, uint256 _id) public {
        vm.assume(_id < 256 && _burner != address(0));
        vm.prank(admin);
        multiBurner.addBurner(_burner, _id);
        assertTrue(multiBurner.canBurn(_burner, _id));
        vm.prank(admin);
        multiBurner.removeBurner(_burner, _id);
        assertFalse(multiBurner.canBurn(_burner, _id));
    }

    function testCannotChangeOwnerByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiBurner.changeOwner(dude);
    }

    function testCanChangeOwner() public {
        vm.prank(admin);
        multiBurner.changeOwner(dude);
        assertTrue(multiBurner.owner() == dude);
    }

    function testCannotPauseByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiBurner.pause();
    }

    function testCannotUnpauseByRandom() public {
        vm.prank(admin);
        multiBurner.pause();
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiBurner.unpause();
    }

    function testBurningPauseUnpause(address _from, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_from));
        vm.prank(admin);
        multiBurner.pause();
        vm.expectRevert("Pausable: paused");
        multiBurner.burn(_from, _id, _amount);
        vm.expectRevert("Pausable: paused");
        multiBurner.burnBatch(_from, ids, amounts);
        vm.prank(admin);
        multiBurner.unpause();
        vm.prank(admin);
        token.mint(_from, _id, _amount, "");
        vm.prank(admin);
        multiBurner.addBurner(burner, _id);
        uint256 _balance = token.balanceOf(_from, _id);
        vm.prank(burner);
        multiBurner.burn(_from, _id, _amount);
        assertTrue(token.balanceOf(_from, _id) == _balance - _amount);
    }

    function ERC1155CompliantReciever(address _addr) public view returns (bool) {
        return _addr != address(0) && _addr != address(token) && _addr != address(multiBurner);
    }
}