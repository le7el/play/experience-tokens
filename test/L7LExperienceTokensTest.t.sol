// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "../src/L7LExperienceTokens.sol";
import "../src/ERC1404Controller.sol";
import "../src/MultiMinter.sol";
import "../src/interface/IERC1155Mintable.sol";
import "../src/interface/IERC1404Controller.sol";

contract L7LExperienceTokensTest is Test {
    L7LExperienceTokens private token;

    address internal admin = address(1);
    address internal dude = address(2);
    address internal minter = address(3);
    uint256 internal constant MAX_INT = 115792089237316195423570985008687907853269984665640564039457584007913129639935;

    uint256[] internal ids = [0, 1, 30, 4, 255];
    uint256[] internal amounts = [0, 100, 1, MAX_INT - 1, 500];

    function setUp() public {
        vm.label(admin, "Admin");
        vm.prank(admin);
        token = new L7LExperienceTokens();
    }

    function testTransferPreventionByDefault(address _from, address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256);
        vm.expectRevert("Not allowed");
        token.safeTransferFrom(_from, _to, _id, _amount, "");
    }

    function testBatchTransferPreventionByDefault(address _from, address _to, uint256[] calldata _ids, uint256[] calldata _amounts) public {
        vm.expectRevert("Not allowed");
        token.safeBatchTransferFrom(_from, _to, _ids, _amounts, "");
    }

    function testTransferWithIdWhitelisting(address _from, address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT / 2 && _from != address(0) && ERC1155CompliantReciever(_to) && _from != _to);
        ERC1404Controller _controller = ERC1404Controller(address(token.trustedController()));
        vm.prank(admin);
        _controller.adminTokenIdWhitelisted(_id, true);
        vm.prank(admin);
        token.mint(_from, _id, _amount, "");
        vm.prank(_from);
        token.safeTransferFrom(_from, _to, _id, _amount, "");
        assertTrue(token.balanceOf(_from, _id) == 0);
        assertTrue(token.balanceOf(_to, _id) == _amount);
        vm.prank(admin);
        token.mint(_from, _id, _amount, "");
        vm.prank(_from);
        token.setApprovalForAll(dude, true);
        vm.prank(dude);
        token.safeTransferFrom(_from, _to, _id, _amount, "");
        assertTrue(token.balanceOf(_from, _id) == 0);
        assertTrue(token.balanceOf(_to, _id) == _amount * 2);
    }

    function testCannotMintIfNotMinter(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.expectRevert("Not a minter");
        token.mint(_to, _id, _amount, "");
    }

    function testCannotBatchMintIfNotMinter(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.expectRevert("Not a minter");
        token.mintBatch(_to, ids, amounts, "");
    }

    function testMint(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
    }

    function testBatchMint(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            uint256 _id = ids[_i];
            uint256 _amount = amounts[_i];
            assertTrue(token.balanceOf(_to, _id) == _amount);
            _i++;
        }
    }

    function testCannotBurnByRandom(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
        vm.prank(dude);
        vm.expectRevert("ERC1155: caller is not token owner, approved contract or burner");
        token.burn(_to, _id, _amount);
    }

    function testBurn(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
        vm.prank(_to);
        token.burn(_to, _id, _amount);
        assertTrue(token.balanceOf(_to, _id) == 0);
    }

    function testApprovedBurn(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
        vm.prank(_to);
        token.setApprovalForAll(dude, true);
        vm.prank(dude);
        token.burn(_to, _id, _amount);
        assertTrue(token.balanceOf(_to, _id) == 0);
    }

    function testAdminBurn(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
        vm.prank(admin);
        token.burn(_to, _id, _amount);
        assertTrue(token.balanceOf(_to, _id) == 0);
    }

    function testCannotBurnBatchByRandom(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == amounts[_i]);
            _i++;
        }
        vm.prank(dude);
        vm.expectRevert("ERC1155: caller is not token owner, approved contract or burner");
        token.burnBatch(_to, ids, amounts);
    }

    function testBurnBatch(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == amounts[_i]);
            _i++;
        }
        vm.prank(_to);
        token.burnBatch(_to, ids, amounts);
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == 0);
            _i++;
        }
    }

    function testAdminBurnBatch(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == amounts[_i]);
            _i++;
        }
        vm.prank(admin);
        token.burnBatch(_to, ids, amounts);
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == 0);
            _i++;
        }
    }

    function testApprovedBurnBatch(address _to) public {
        vm.assume(ERC1155CompliantReciever(_to));
        vm.prank(admin);
        token.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == amounts[_i]);
            _i++;
        }
        vm.prank(_to);
        token.setApprovalForAll(dude, true);
        vm.prank(dude);
        token.burnBatch(_to, ids, amounts);
        for (uint256 _i = 0; _i < ids.length;) {
            assertTrue(token.balanceOf(_to, ids[_i]) == 0);
            _i++;
        }
    }

    function testCannotChangeMinterByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not a minter");
        token.changeMinter(dude);
    }

    function testAssignMultiMinter() public {
        MultiMinter multiMinter = new MultiMinter(admin, IERC1155Mintable(address(token)));
        vm.prank(admin);
        token.changeMinter(address(multiMinter));
        assertTrue(token.trustedMinter() == address(multiMinter));
    }

    function testCannotChangeBurnerByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not a burner");
        token.changeBurner(dude);
    }

    function testChangeBurner() public {
        vm.prank(admin);
        token.changeBurner(dude);
        assertTrue(token.trustedBurner() == dude);
    }

    function testCannotChangeControllerByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not a controller");
        token.changeController(dude);
    }

    function testChangingController() public {
        ERC1404Controller _controller = new ERC1404Controller(admin);
        IERC1404Controller _currentController = token.trustedController();
        assertTrue(address(_controller) != address(_currentController));
        vm.prank(admin);
        _currentController.adminChangeController(address(_controller));
        assertTrue(address(token.trustedController()) == address(_controller));
    }

    function ERC1155CompliantReciever(address _addr) public view returns (bool) {
        return _addr != address(0) && _addr != address(token) && _addr != address(token.trustedController());
    }
}
  