// SPDX-License-Identifier: MPL-2.0
pragma solidity ^0.8.10;

import "forge-std/Test.sol";
import "../src/MultiMinter.sol";
import "../src/L7LExperienceTokens.sol";

contract MultiMinterTest is Test {
    MultiMinter private multiMinter;
    L7LExperienceTokens private token;

    address internal admin = address(1);
    address internal dude = address(2);
    address internal minter = address(3);

    uint256 internal constant MAX_INT = 115792089237316195423570985008687907853269984665640564039457584007913129639935;

    uint256[] internal ids = [0, 1, 30, 4, 255];
    uint256[] internal amounts = [0, 100, 1, MAX_INT - 1, 500];

    function setUp() public {
        vm.label(admin, "Admin");
        vm.prank(admin);
        L7LExperienceTokens _mintable = new L7LExperienceTokens();
        token = _mintable;
        vm.prank(admin);
        multiMinter = new MultiMinter(admin, _mintable);
        vm.prank(admin);
        token.changeMinter(address(multiMinter));
    }

    function testMintWithMultiMinter(address _to, uint256 _id, uint256 _amount) public {
        // Ensure that id = 4 won't overflow as well as other values with 500 being the max increment
        vm.assume(_id < 256 && _id != 4 && _amount < MAX_INT - 500 && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        multiMinter.addMinter(minter, _id);
        vm.prank(minter);
        multiMinter.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
        for (uint256 _i = 0; _i < ids.length;) {
            vm.prank(admin);
            multiMinter.addMinter(minter, ids[_i]);
            _i++;
        }
        vm.prank(minter);
        multiMinter.mintBatch(_to, ids, amounts, "");
        for (uint256 _i = 0; _i < ids.length;) {
            uint256 __amount = amounts[_i];
            if (ids[_i] == _id) __amount += _amount;
            assertTrue(token.balanceOf(_to, ids[_i]) == __amount);
            _i++;
        }
    }

    function testCannotAddAndRemovingMintersByRandom(address _minter, uint256 _id) public {
        vm.assume(_id < 256);
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiMinter.addMinter(_minter, _id);
        assertFalse(multiMinter.canMint(_minter, _id));
        vm.prank(admin);
        multiMinter.addMinter(_minter, _id);
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiMinter.removeMinter(_minter, _id);
        assertTrue(multiMinter.canMint(_minter, _id));
    }

    function testAddingAndRemovingNewMinters(address _minter, uint256 _id) public {
        vm.assume(_id < 256 && _minter != address(0));
        vm.prank(admin);
        multiMinter.addMinter(_minter, _id);
        assertTrue(multiMinter.canMint(_minter, _id));
        vm.prank(admin);
        multiMinter.removeMinter(_minter, _id);
        assertFalse(multiMinter.canMint(_minter, _id));
    }

    function testCannotChangeOwnerByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiMinter.changeOwner(dude);
    }

    function testCanChangeOwner() public {
        vm.prank(admin);
        multiMinter.changeOwner(dude);
        assertTrue(multiMinter.owner() == dude);
    }

    function testCannotPauseByRandom() public {
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiMinter.pause();
    }

    function testCannotUnpauseByRandom() public {
        vm.prank(admin);
        multiMinter.pause();
        vm.prank(dude);
        vm.expectRevert("Not owner");
        multiMinter.unpause();
    }

    function testMintingPauseUnpause(address _to, uint256 _id, uint256 _amount) public {
        vm.assume(_id < 256 && _amount < MAX_INT && ERC1155CompliantReciever(_to));
        vm.prank(admin);
        multiMinter.pause();
        vm.prank(admin);
        vm.expectRevert("Pausable: paused");
        multiMinter.mint(_to, _id, _amount, "");
        vm.expectRevert("Pausable: paused");
        multiMinter.mintBatch(_to, ids, amounts, "");
        vm.prank(admin);
        multiMinter.unpause();
        vm.prank(admin);
        multiMinter.mint(_to, _id, _amount, "");
        assertTrue(token.balanceOf(_to, _id) == _amount);
    }

    function ERC1155CompliantReciever(address _addr) public view returns (bool) {
        return _addr != address(0) && _addr != address(token) && _addr != address(multiMinter);
    }
}