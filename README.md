# Experience tokens

ERC1155 tokens to grant experience to level up LE7EL NFTs. Supports multiple minters, so you can dedicate specific contracts to mint selected experience tokens.

# Getting started

Install [Foundry](https://getfoundry.sh/).

Run tests: `forge test`

# Client utils

#### Getting started

```js
import { assertExpTokensContractDeployed, assertMultiMinterContractDeployed, getPXPTokenId } from '@le7el/experience_tokens'
```

#### assertExpTokensContractDeployed(networkId: string, version: string): Promise <string>

Ensures that specific contract version is deployed on given network
```js
assertExpTokensContractDeployed(5, 'v1').then(console.log)
// "0xA0FE716a07be4C0880EA8E8bEae9098D0aF8c32C"
```

#### assertMultiMinterContractDeployed(networkId: string, version: string): Promise <string>

Ensures that specific contract version is deployed on given network
```js
assertMultiMinterContractDeployed(5, 'v1').then(console.log)
// "0xF526929CF357842Eb0aEB76Ff58d3010EF35bB62"
```

#### getPXPTokenId(version: string): Promise< number >
Returns id of PXP token for given verison
```js
getPXPTokenId('v1').then(console.log)
// 0
```

# Deployments

To deploy, run scripts with the relevant environment variables: `source .secret && forge script script/L7LExperienceTokens.s.sol:L7LExperienceTokensScript --rpc-url $SEPOLIA_RPC_URL  --private-key $PRIVATE_KEY --broadcast --slow --verify --etherscan-api-key $ETHERSCAN_KEY -vvvv`

`source .secret && TOKEN=0x10Ae65AF15a6A6885575193f58488C58721163a0 forge script script/MultiMinter.s.sol:MultiMinterScript --rpc-url $SEPOLIA_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --slow --etherscan-api-key $ETHERSCAN_KEY -vvvv`

`source .secret && TOKEN=0x10Ae65AF15a6A6885575193f58488C58721163a0 forge script script/MultiBurner.s.sol:MultiBurnerScript --rpc-url $SEPOLIA_RPC_URL  --private-key $PRIVATE_KEY --broadcast --verify --slow --etherscan-api-key $ETHERSCAN_KEY -vvvv`

And update `js/contracts.js` with corresponding changes as well as publish new version to npm.

## Rinkeby

* L7LExperienceTokens deployed to: `0xE4d52aa9d5b1dCBa0Ec24CA6B7305a609167Bb3e`;
* MultiMinter deployed to: `0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F`.

## Goerli

* L7LExperienceTokens deployed to: `0xA0FE716a07be4C0880EA8E8bEae9098D0aF8c32C`;
* MultiMinter deployed to: `0xF526929CF357842Eb0aEB76Ff58d3010EF35bB62`;
* MultiBurner deployed to: `0x4AF633ef5d8F8AB22c50BF2C4a2b792257D43107`;

## Sepolia

* L7LExperienceTokens deployed to: `0x10Ae65AF15a6A6885575193f58488C58721163a0`;
* MultiMinter deployed to: `0x914A428404657CA547085Ec0Ee7Ac6f948f99A4E`;
* MultiBurner deployed to: `0x98d6202f0f4440a083A2F68Eebed1bAc5EE9AA55`;

## Polygon

* L7LExperienceTokens deployed to: `0x1ac8f959e46Bb3B0780c2A101e95F672F9575a72`;
* MultiMinter deployed to: `0xA48A15010Eb05Eb13582E673C14eeD05cD708F29`;
* MultiBurner deployed to: `0x51F0490C2c251d256471F18F05a12FE5bA7eb427`;