export default {
  '4': {
    'v1': {
      exp_tokens: "0xE4d52aa9d5b1dCBa0Ec24CA6B7305a609167Bb3e",
      multi_minter: "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F",
    }
  },
  '5': {
    'v1': {
      exp_tokens: "0xA0FE716a07be4C0880EA8E8bEae9098D0aF8c32C",
      multi_minter: "0xF526929CF357842Eb0aEB76Ff58d3010EF35bB62",
      multi_burner: "0x4AF633ef5d8F8AB22c50BF2C4a2b792257D43107"
    }
  },
  '11155111': {
    'v1': {
      exp_tokens: "0x10Ae65AF15a6A6885575193f58488C58721163a0",
      multi_minter: "0x914A428404657CA547085Ec0Ee7Ac6f948f99A4E",
      multi_burner: "0x98d6202f0f4440a083A2F68Eebed1bAc5EE9AA55"
    }
  },
  '137': {
    'v1': {
      exp_tokens: "0x1ac8f959e46Bb3B0780c2A101e95F672F9575a72",
      multi_minter: "0xA48A15010Eb05Eb13582E673C14eeD05cD708F29",
      multi_burner: "0x51F0490C2c251d256471F18F05a12FE5bA7eb427"
    }
  }
}

export const CURRENT_VERSION = 'v1'

export const tokens = {
  'PXP': {
    'v1': 0
  }
}