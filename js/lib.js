import contracts, { tokens, CURRENT_VERSION } from "./contracts"

const _assertContractDeployed = (networkId, contractName, version = CURRENT_VERSION) => {
  if (contracts[`${networkId}`] && contracts[`${networkId}`][version] && contracts[`${networkId}`][version][contractName]) {
    return Promise.resolve(contracts[networkId][version][contractName])
  }
  return Promise.reject(`contract ${contractName}:${version} is not deployed to network ${networkId}`);
}

const assertExpTokensContractDeployed = (networkId, version = CURRENT_VERSION) => {
  return _assertContractDeployed(networkId, 'exp_tokens', version)
}

const assertMultiMinterContractDeployed = (networkId, version = CURRENT_VERSION) => {
  return _assertContractDeployed(networkId, 'multi_minter', version)
}

const getPXPTokenId = (version = CURRENT_VERSION) => {
  if (tokens['PXP'] && tokens['PXP'][version] !== undefined) {
    return Promise.resolve(tokens['PXP'][version])
  }
  return Promise.reject(`PXP token is not supported in contract version: ${version}`);
}

export {
  assertExpTokensContractDeployed,
  assertMultiMinterContractDeployed,
  getPXPTokenId,
}